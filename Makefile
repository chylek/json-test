LIBS=$(shell find . -mindepth 1 -maxdepth 1 -type d | grep -Ev ".git|template|results" | cut -b3-)
LIBS_CLEAN=$(patsubst %,%-clean,$(LIBS))

all: $(LIBS)

clean: $(LIBS_CLEAN)

$(LIBS): %:
	make -C $@

$(LIBS_CLEAN): %:
	make -C $(shell echo $@ | sed 's/-clean//') clean

.PHONY: all clean $(LIBS) $(LIBS_CLEAN)
