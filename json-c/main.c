#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <json.h>

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096];
    const char* mountpoint, *include_str;
    size_t done = 0, i, len;
    ssize_t bytes;
    struct json_object *json = NULL, *obj, *include;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json = json_tokener_parse(buff);

    if(json_object_get_type(json) != json_type_object) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    /* struct json_object* json_obect_object_get(struct json_object *j_obj, const char*)
     * is deprecated */
    if(json_object_object_get_ex(json, "mountpoint", &obj) == FALSE) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(json_object_get_type(obj) != json_type_string) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = json_object_get_string(obj);
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    if(json_object_object_get_ex(json, "open limit", &obj) == FALSE) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(json_object_get_type(obj) != json_type_int) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = json_object_get_int(obj);
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    if(json_object_object_get_ex(json, "debug", &obj) == FALSE) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(json_object_get_type(obj) != json_type_boolean) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = json_object_get_int(obj);
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    if(json_object_object_get_ex(json, "include", &obj) == FALSE) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(json_object_get_type(obj) != json_type_array) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            len = json_object_array_length(obj);
            printf("Include:");
            for(i=0; i<len; i++) {
                include = json_object_array_get_idx(obj, i);
                if(json_object_get_type(include) != json_type_string) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = json_object_get_string(include);
                    printf("%s '%s'", i ? "," : "", include_str);
                }
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        json_object_put(json);
    }
    return ret;
}

