#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "json.h"

static json_t* my_json_get_key (json_t *json, const char *key) {
    json = json->child;

    while(json) {
        if(!strcmp(json->text, key)) {
            return json->child;
        }
        json = json->next;
    }

    return NULL;
} /* my_json_get_key */

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096], *endptr;
    const char* mountpoint, *include_str;
    size_t done = 0;
    ssize_t bytes;
    json_t *json = NULL, *obj, *include;
    enum json_error err;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    err = json_parse_document(&json, buff);
    if(err != JSON_OK) {
        ret = -1;
        fprintf(stderr, "ERROR: Cannot parse json\n");
        goto main_err;
    }

    if(json->type != JSON_OBJECT) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    obj = my_json_get_key(json, "mountpoint");
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(obj->type != JSON_STRING) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = obj->text;
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = my_json_get_key(json, "open limit");
    if(!obj) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(obj->type != JSON_NUMBER) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = strtoll(obj->text, &endptr, 10);
            if(*endptr) {
                fprintf(stderr, "ERROR: Cannot get open limit: "
                        "Cannot convert '%s' to number: %s\n",
                        obj->text, strerror(errno));
            } else {
                printf("Open limit: '%d'\n", open_limit);
            }
        }
    }

    obj = my_json_get_key(json, "debug");
    if(!obj) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(obj->type != JSON_TRUE && obj->type != JSON_FALSE) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = (obj->type == JSON_TRUE);
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = my_json_get_key(json, "include");
    if(!obj) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(obj->type != JSON_ARRAY) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            printf("Include:");
            include = obj->child;
            while(include) {
                if(include->type != JSON_STRING) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = include->text;
                    printf("%s '%s'", include != obj->child ? "," : "", include_str);
                }
                include = include->next;
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        json_free_value(&json);
    }
    return ret;
}

