#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "yajl/yajl_tree.h"

#define ENTRY_SIZE      128

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096], errbuff[2014];
    const char *mountpoint_path[] = {"mountpoint", NULL}, *open_limit_path[] = {"open limit", NULL}, *debug_path[] = {"debug", NULL}, *include_path[] = {"include", NULL};
    const char *mountpoint, *include_str;
    size_t done = 0, i;
    ssize_t bytes;
    yajl_val json, obj, include;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff, sizeof(buff));
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json = yajl_tree_parse(buff, errbuff, sizeof(errbuff));
    if(!json) {
        ret = -1;
        fprintf(stderr, "Cannot parse json: %s\n", errbuff);
        goto main_err;
    }

    if(!YAJL_IS_OBJECT(json)) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    obj = yajl_tree_get(json, mountpoint_path, yajl_t_any);
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(!YAJL_IS_STRING(obj)) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = YAJL_GET_STRING(obj);
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = yajl_tree_get(json, open_limit_path, yajl_t_any);
    if(!obj) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(!YAJL_IS_INTEGER(obj)) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = YAJL_GET_INTEGER(obj);
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    obj = yajl_tree_get(json, debug_path, yajl_t_any);
    if(!obj) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(!YAJL_IS_TRUE(obj) && !YAJL_IS_FALSE(obj)) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = YAJL_IS_TRUE(obj);
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = yajl_tree_get(json, include_path, yajl_t_any);
    if(!obj) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(!YAJL_IS_ARRAY(obj)) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            printf("Include:");
            for(i=0; i<obj->u.array.len; i++) {
                include = obj->u.array.values[i];
                if(!YAJL_IS_STRING(include)) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = YAJL_GET_STRING(include);
                    printf("%s '%s'", i ? "," : "", include_str);
                }
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        yajl_tree_free(json);
    }
    return ret;
}

