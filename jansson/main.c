#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <jansson.h>

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096];
    const char* mountpoint, *include_str;
    size_t done = 0, i, len;
    ssize_t bytes;
    json_t *json = NULL, *obj, *include;
    json_error_t json_err;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json = json_loads(buff, 0, &json_err);
    if(!json) {
        fprintf(stderr, "ERROR: Cannot parse json: %s (line: %d)\n", json_err.text, json_err.line);
        ret = ENOMSG;
        goto main_err;
    }

    if(!json_is_object(json)) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    obj = json_object_get(json, "mountpoint");
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(!json_is_string(obj)) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = json_string_value(obj);
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = json_object_get(json, "open limit");
    if(!obj) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(!json_is_integer(obj)) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = json_integer_value(obj);
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    obj = json_object_get(json, "debug");
    if(!obj) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(!json_is_boolean(obj)) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = json_boolean_value(obj);
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = json_object_get(json, "include");
    if(!obj) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(!json_is_array(obj)) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            len = json_array_size(obj);
            printf("Include:");
            for(i=0; i<len; i++) {
                include = json_array_get(obj, i);
                if(!json_is_string(include)) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = json_string_value(include);
                    printf("%s '%s'", i ? "," : "", include_str);
                }
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        json_decref(json);
    }
    return ret;
}

