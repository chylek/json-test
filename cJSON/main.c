#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "cJSON.h"

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096];
    const char* mountpoint, *include_str;
    size_t done = 0, len, i;
    ssize_t bytes;
    cJSON *json = NULL, *obj, *include;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json =  cJSON_Parse(buff);
    if(!json) {
        fprintf(stderr, "ERROR: Cannot parse json. Error before: %s\n", cJSON_GetErrorPtr());
        ret = -1;
        goto main_err;
    }

    if(!cJSON_IsObject(json)) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    obj = cJSON_GetObjectItemCaseSensitive(json, "mountpoint");
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(!cJSON_IsString(obj)) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = obj->valuestring;
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = cJSON_GetObjectItemCaseSensitive(json, "open limit");
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(!cJSON_IsNumber(obj)) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = obj->valueint;
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    obj = cJSON_GetObjectItemCaseSensitive(json, "debug");
    if(!obj) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(!cJSON_IsBool(obj)) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = obj->valueint;
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = cJSON_GetObjectItemCaseSensitive(json, "include");
    if(!obj) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(!cJSON_IsArray(obj)) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            len = cJSON_GetArraySize(obj);
            printf("Include:");
            for(i=0; i<len; i++) {
                include = cJSON_GetArrayItem(obj, i);
                if(!cJSON_IsString(include)) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = include->valuestring;
                    printf("%s '%s'", i ? "," : "", include_str);
                }
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        cJSON_Delete(json);
    }
    return ret;
}

