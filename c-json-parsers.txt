    YAJL
> valgrind: total heap usage: 65 allocs, 65 frees, 8,319 bytes allocated

+ Available by package manager (Arch, Ubuntu 16.04)
+ Supports JSON generation

- Advanced API to do simple task
- Predefined array with 'path' as the key to get the values



    cJSON
> valgrind: total heap usage: 27 allocs, 27 frees, 5,041 bytes allocated

+ Easy to integrate with project: 2 files: .h and .c

- Compiler warning: "comparing floating point with == or != is unsafe" (x3)
- C++ like API naming
- Missing JSON generation



    frozen
> valgrind: total heap usage: 2 allocs, 2 frees, 4,105 bytes allocated

+ Easy to integrate with project: 2 files: .h and .c
+ Supports JSON generation

- Does not support spaces in keys names
- Has a problem with uninitialzed int scanned as bool
- Not intuitive scanf / printf like API
- Compiler warnings:
    "cast discards 'const' qualifier from pointer target type"
    "format not a string literal, argument types not checked"
  Repeated several times



    jansson
> valgrind: total heap usage: 37 allocs, 37 frees, 5,605 bytes allocated

+ Available by package manager (Arch, Ubuntu 16.04)
+ Supports JSON generation

- valgrind: WARNING: Serious error when reading debug info
            When reading debug info from /usr/lib/x86_64-linux-gnu/libjansson.so.4.7.0:
            Ignoring non-Dwarf2/3/4 block in .debug_info



    json-c
> valgrind: total heap usage: 42 allocs, 42 frees, 8,252 bytes allocated

+ Available by package manager (Arch, Ubuntu 16.04)
+ Supports JSON generation

- Redefines 'TRUE' and 'FALSE' under co conditions
- "json_object_object_*" API naming which is little unreadable



    json-parser
> valgrind: total heap usage: 20 allocs, 20 frees, 4,971 bytes allocated
> Supports JSON generation by json-builder library

+ Easy to integrate with project: 2 files: .h and .c
+ Can also be integrated as static library by building sources with autotools

- Does not have ability to get value by key
- Requires additional -lm flag, not mentioned anywhere
- Compiler warning: "this statement may fall through"



    microjson
> valgrind: total heap usage: 0 allocs, 0 frees, 0 bytes allocated

+ Easy to integrate with project: 2 files: .h and .c
+ No memory allocations

- A little complicated API to use
- Requires all keys to be defined as variables before parsing the JSON
- Compiler warnings:
    "initialization discards 'const' qualifier from pointer target type" (as many as keys to collect)
    "enumeration value 't_time' not handled in switch"
- Missing JSON generation



    mjson
> valgrind: total heap usage: 82 allocs, 82 frees, 6,218 bytes allocated

+ Easy to integrate with project: 2 files: .h and .c
+ Easy to integrate with project: create libmjson.a and copy it with header
+ Supports JSON generation

- Does not have ability to get value by key



    nxjson
> valgrind: total heap usage: 13 allocs, 13 frees, 4,960 bytes allocated

+ Easy to integrate with project: 2 files: .h and .c
+ Works in-place damaging input string

- compiler warning: "cast discards 'const' qualifier from pointer target type"
- Missing JSON generation



    ojc
> valgrind: total heap usage: 49 allocs, 49 frees, 6,656 bytes allocated

+ Easy to integrate with project: create libojc.a and copy it with header
+ Supports JSON generation

- Requires additional -lm flag, not mentioned anywhere



    parson
> valgrind: total heap usage: 58 allocs, 58 frees, 5,420 bytes allocated

+ Easy to integrate with project: 2 files: .h and .c
+ Supports JSON generation

- Uses two types of structure to handle JSON (object and value)
- Compiler warnings:
    "to be safe all intermediate pointers in cast from 'char **' to 'const char **' must be 'const' qualified"
    "comparing floating point with == or != is unsafe"



================================================================================

http://www.json.org/

================================================================================
================================= Dropped libs =================================
================================================================================

    JSON_checker
JSON validation tool

    libu
json handling is one of 12 modules

    jsonsl
Stream dedicated

    WJElement
"Built on top of the lower-level WJReader and WJWriter libraries"
We need simple library, as far as possible, without dependencies

    ujson4c
It is a wrapper for another JSON library to provide more user friendly
interface. Using 3rd party software which uses another 3rd party software
is wrong approach.

    jsmn
Holds offset of each value within the json string. User has to extract
everything on his own.
