#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "mjson.h"

#define sizeof_arr(arr) (sizeof(arr) / sizeof(arr[0]))
#define INCLUDE_PATTERN_LEN 128

static int open_limit;
static char mountpoint[JSON_VAL_MAX];
static bool debug;
static char *include[2], str_store[sizeof_arr(include) * INCLUDE_PATTERN_LEN];

static const struct json_attr_t json_attrs[] = {
    {"mountpoint",  t_string, .addr.string = mountpoint, .len = sizeof(mountpoint)},
    {"open limit",  t_integer, .addr.integer = &open_limit},
    {"debug",       t_boolean, .addr.boolean = &debug},
    {"include",     t_array, .addr.array.element_type = t_string,
                             .addr.array.maxlen = sizeof_arr(include),
                             .addr.array.arr.strings.ptrs = (char**)include,
                             .addr.array.arr.strings.store = str_store,
                             .addr.array.arr.strings.storelen = sizeof(str_store)
                            },
    {NULL},
};

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1;
    char buff[4096];
    const char *endptr;
    size_t done = 0, i, len = sizeof_arr(include);
    ssize_t bytes;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    if((ret = json_read_object(buff, json_attrs, &endptr))) {
        fprintf(stderr, "ERROR: Cannot parse json: %s\n%s",
                json_error_string(ret), endptr);
        goto main_err;
    }

    printf("Mountpoint: '%s'\n", mountpoint);
    printf("Open limit: '%d'\n", open_limit);
    printf("Debug: '%s'\n", debug ? "ON" : "OFF");
    printf("Include:");
    for(i=0; i<len; i++) {
        printf("%s '%s'", i ? "," : "", include[i]);
    }
    printf("\n");

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    return ret;
}

