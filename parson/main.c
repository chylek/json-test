#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "parson.h"

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096];
    const char* mountpoint, *include_str;
    size_t done = 0, i, len;
    ssize_t bytes;
    JSON_Object *json_o;
    JSON_Value *json = NULL, *obj, *include;
    JSON_Array *array;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json = json_parse_string(buff);
    if(!json) {
        fprintf(stderr, "ERROR: Cannot parse json\n");
        ret = ENOMSG;
        goto main_err;
    }

    if(json_value_get_type(json) != JSONObject) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }
    json_o = json_value_get_object(json);
    if(!json_o) {
        fprintf(stderr, "ERROR: Cannot extract object from value\n");
        ret = ENOMSG;
        goto main_err;
    }

    obj = json_object_get_value(json_o, "mountpoint");
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(json_value_get_type(obj) != JSONString) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = json_value_get_string(obj);
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = json_object_get_value(json_o, "open limit");
    if(!obj) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(json_value_get_type(obj) != JSONNumber) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = json_value_get_number(obj);
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    obj = json_object_get_value(json_o, "debug");
    if(!obj) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(json_value_get_type(obj) != JSONBoolean) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = json_value_get_boolean(obj);
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = json_object_get_value(json_o, "include");
    if(!obj) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(json_value_get_type(obj) != JSONArray) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            array = json_value_get_array(obj);
            len = json_array_get_count(array);
            printf("Include:");
            for(i=0; i<len; i++) {
                include = json_array_get_value(array, i);
                if(json_value_get_type(include) != JSONString) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = json_value_get_string(include);
                    printf("%s '%s'", i ? "," : "", include_str);
                }
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        json_value_free(json);
    }
    return ret;
}

