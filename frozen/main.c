#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "frozen.h"

int main (int argc, char **argv) {
    int ret = ENOMSG, i, fd = -1, open_limit, debug = 0;
    char buff[4096];
    char* mountpoint = NULL;
    size_t done = 0;
    ssize_t bytes;
    struct json_token include;
    void *handle = NULL;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    if(json_scanf(buff, done, "{mountpoint: %Q}", &mountpoint) == 0) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        printf("Mountpoint: '%s'\n", mountpoint);
    }

    /* This one does not work - library requires no spaces in keys names */
    if(json_scanf(buff, done, "{open limit: %d}", &open_limit) == 0) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        printf("Open limit: '%d'\n", open_limit);
    }

    /* If debug were not set at the beginning, this reports "Conditional
     * jump or move depends on uninitialised value(s)" in valgrind */
    if(json_scanf(buff, done, "{debug: %B}", &debug) == 0) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        printf("Debug: '%s'\n", debug ? "ON" : "OFF");
    }

    printf("Include:");
    handle = json_next_elem(buff, done, handle, ".include" , &i, &include);
    while(handle) {
        if(include.type != JSON_TYPE_STRING) {
            fprintf(stderr, "ERROR: Non-string value in include array\n");
        } else {
            printf("%s '%.*s'", i ? "," : "", include.len, include.ptr);
        }
        handle = json_next_elem(buff, done, handle, ".include" , &i, &include);
    }
    printf("\n");

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    free(mountpoint);
    return ret;
}

