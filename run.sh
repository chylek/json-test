#!/bin/sh

SCRIPT_NAME=$(basename "${0}")

RUN_TESTS=0
CLEAN=0

CLEAN_SUFFIX="-clean"
RESULTS_DIR="results"
SUMMARY_NAME="summary"
MAKE_NAME="compile"
OUTPUT_NAME="output"
VALGRIND_NAME="valgrind"

VALGRIND_ARGS="--leak-check=full --show-leak-kinds=all --track-origins=yes --partial-loads-ok=yes --track-fds=yes"

show_help () {
    echo "Run json libraries tests"
    echo ""
    echo "Usage: ${SCRIPT_NAME} [OPTIONS]"
    echo ""
    echo "Available OPTIONS:"
    echo "  -h, --help    Show this message"
    echo "  -t, --test    Run json libraries tests"
    echo "  -c, --clean   Clean tests results"
}

run_make () { # LIB_NAME
    LIB_NAME="${1}"

    OUTPUT_DIR="${RESULTS_DIR}/${LIB_NAME}"
    make "${LIB_NAME}${CLEAN_SUFFIX}" >/dev/null 2>&1
    make "${LIB_NAME}" > "${OUTPUT_DIR}/${MAKE_NAME}" 2>&1
}

run_normal () { # LIB_NAME
    LIB_NAME="${1}"
    OUTPUT_DIR="${RESULTS_DIR}/${LIB_NAME}"

    ${LIB_NAME}/main "test.json" > "${OUTPUT_DIR}/${OUTPUT_NAME}" 2>&1
}

run_valgrind () { # LIB_NAME
    LIB_NAME="${1}"
    OUTPUT_DIR="${RESULTS_DIR}/${LIB_NAME}"

    valgrind ${VALGRIND_ARGS} "${LIB_NAME}/main" "test.json" > "${OUTPUT_DIR}/${VALGRIND_NAME}" 2>&1
}

if [ ${#} -eq 0 ]; then
    show_help
    exit 0
fi

for ARG in ${@}; do
    case "${ARG}" in
        -h|--help)
            show_help
            exit 0
            ;;
        -t|--test)
            RUN_TESTS=1
            ;;
        -c|--clean)
            CLEAN=1
            ;;
        *)
            echo "ERROR: Unknown option: ${ARG}"
            exit 1
            ;;
    esac
done

if [ ${RUN_TESTS} -eq 1 ]; then
    # Clean everything before running tests to avoid garbage
    CLEAN=1
fi

if [ ${CLEAN} -eq 1 ]; then
    echo "Removing tests results directory"
    rm -rf "${RESULTS_DIR}"
    echo "Running 'clean' target in makefiles"
    make clean >/dev/null 2>&1
fi

if [ ${RUN_TESTS} -eq 1 ]; then
    mkdir "${RESULTS_DIR}"

    for LIB in $(find -mindepth 1 -maxdepth 1 -type d | grep -Ev ".git|template|results" | cut -b3- | sort -f); do
        echo "Running test for '${LIB}'"
        mkdir "${RESULTS_DIR}/${LIB}"
        run_make "${LIB}"
        if [ -e "${LIB_NAME}/main" ]; then
            run_normal "${LIB}"
            run_valgrind "${LIB}"
        fi

        echo -n "${LIB} output: " >> "${RESULTS_DIR}/${SUMMARY_NAME}"
        if [ ! -f "${RESULTS_DIR}/${LIB}/${OUTPUT_NAME}" ]; then
            echo "NO OUTPUT" >> "${RESULTS_DIR}/${SUMMARY_NAME}"
        else
            OUTPUT_DIFF=$(diff "test_output_correct" "${RESULTS_DIR}/${LIB}/${OUTPUT_NAME}")
            if [ $(echo "${OUTPUT_DIFF}" | wc -l) -gt 1 ]; then
                echo "WRONG" >> "${RESULTS_DIR}/${SUMMARY_NAME}"
            else
                echo "CORRECT" >> "${RESULTS_DIR}/${SUMMARY_NAME}"
            fi
        fi
    done
fi

