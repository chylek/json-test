#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "nxjson.h"

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096];
    const char* mountpoint, *include_str;
    size_t done = 0, i, len;
    ssize_t bytes;
    const nx_json *json = NULL, *obj, *include;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json = nx_json_parse(buff, NULL);
    if(!json) {
        ret = -1;
        fprintf(stderr, "ERROR: Cannot parse json\n");
        goto main_err;
    }

    if(json->type != NX_JSON_OBJECT) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    obj = nx_json_get(json, "mountpoint");
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(obj->type != NX_JSON_STRING) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = obj->text_value;
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = nx_json_get(json, "open limit");
    if(!obj) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(obj->type != NX_JSON_INTEGER) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = obj->int_value;
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    obj = nx_json_get(json, "debug");
    if(!obj) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(obj->type != NX_JSON_BOOL) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = obj->int_value;
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = nx_json_get(json, "include");
    if(!obj) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(obj->type != NX_JSON_ARRAY) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            len = obj->length;
            printf("Include:");
            for(i=0; i<len; i++) {
                include = nx_json_item(obj, i);
                if(include->type != NX_JSON_STRING) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = include->text_value;
                    printf("%s '%s'", i ? "," : "", include_str);
                }
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        nx_json_free(json);
    }
    return ret;
}

