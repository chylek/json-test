#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "ojc/ojc.h"

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096];
    const char* mountpoint, *include_str;
    size_t done = 0, i, len;
    ssize_t bytes;
    ojcVal json, obj, include;
    struct _ojcErr err;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    ojc_err_init(&err);

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json = ojc_parse_str(&err, buff, NULL, NULL);

    if(ojc_type(json) != OJC_OBJECT) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    obj = ojc_object_get_by_key(&err, json, "mountpoint");
    if(err.code != OJC_OK) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(ojc_type(obj) != OJC_STRING) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = ojc_str(&err, obj);
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = ojc_object_get_by_key(&err, json, "open limit");
    if(err.code != OJC_OK) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(ojc_type(obj) != OJC_FIXNUM) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = ojc_int(&err, obj);
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    obj = ojc_object_get_by_key(&err, json, "debug");
    if(err.code != OJC_OK) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(ojc_type(obj) != OJC_TRUE && ojc_type(obj) != OJC_FALSE) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = ojc_bool(&err, obj);
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = ojc_object_get_by_key(&err, json, "include");
    if(err.code != OJC_OK) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(ojc_type(obj) != OJC_ARRAY) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            len = ojc_member_count(&err, obj);
            include = ojc_members(&err, obj);
            printf("Include:");
            for(i=0; i<len; i++) {
                if(ojc_type(include) != OJC_STRING) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = ojc_str(&err, include);
                    printf("%s '%s'", i ? "," : "", include_str);
                }
                include = ojc_next(include);
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    ojc_destroy(json);
    ojc_cleanup();
    return ret;
}

