#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>

#include "json.h"

static json_value* my_json_get_key (json_value *json, const char *key) {
    int i, obj_len;
    size_t key_len;
    json_object_entry *obj;

    key_len = strlen(key);

    obj_len = json->u.object.length;
    for(i=0; i<obj_len; i++) {
        obj = &json->u.object.values[i];
        if(key_len == obj->name_length &&
           !strncmp(obj->name, key, key_len)) {
            return obj->value;
        }
    }

    return NULL;
} /* my_json_get_key */

int main (int argc, char **argv) {
    int ret = ENOMSG, fd = -1, open_limit, debug;
    char buff[4096];
    const char* mountpoint, *include_str;
    size_t done = 0, i, len;
    ssize_t bytes;
    json_value *json = NULL, *obj, *include;

    if(argc != 2) {
        fprintf(stderr, "ERORR: Wrong number of arguments\n");
        return EINVAL;
    }

    fd = open(argv[1], O_RDONLY);
    if(fd < 0) {
        ret = errno;
        fprintf(stderr, "ERROR: Cannot open '%s' file: %s\n", argv[1], strerror(errno));
        return ret;
    }

    bytes = read(fd, buff + done, sizeof(buff) - done);
    done += bytes;
    while(bytes && done < sizeof(buff)) {
        if(bytes < 0) {
            ret = errno;
            fprintf(stderr, "ERROR: Cannot read '%s' file: %s\n", argv[1], strerror(errno));
            goto main_err;
        }
        bytes = read(fd, buff + done, sizeof(buff) - done);
        done += bytes;
    }
    if(done < sizeof(buff)) {
        buff[done] = '\0';
        done++;
    } else {
        ret = ENOBUFS;
        fprintf(stderr, "ERROR: Buffer too small\n");
        goto main_err;
    }

    json = json_parse((json_char*)buff, done);
    if(!json) {
        ret = -1;
        fprintf(stderr, "ERROR: Cannot parse json\n");
        goto main_err;
    }

    if(json->type != json_object) {
        fprintf(stderr, "ERROR: Given json is not an object\n");
        ret = EINVAL;
        goto main_err;
    }

    obj = my_json_get_key(json, "mountpoint");
    if(!obj) {
        fprintf(stderr, "ERROR: Mountpoint not found\n");
    } else {
        if(obj->type != json_string) {
            fprintf(stderr, "ERROR: Mountpoint is not a string\n");
        } else {
            mountpoint = obj->u.string.ptr;
            printf("Mountpoint: '%s'\n", mountpoint);
        }
    }

    obj = my_json_get_key(json, "open limit");
    if(!obj) {
        fprintf(stderr, "ERROR: Open limit not found\n");
    } else {
        if(obj->type != json_integer) {
            fprintf(stderr, "ERROR: Open limit is not an integer\n");
        } else {
            open_limit = obj->u.integer;
            printf("Open limit: '%d'\n", open_limit);
        }
    }

    obj = my_json_get_key(json, "debug");
    if(!obj) {
        fprintf(stderr, "ERROR: Debug not found\n");
    } else {
        if(obj->type != json_boolean) {
            fprintf(stderr, "ERROR: Debug is not a boolean\n");
        } else {
            debug = obj->u.boolean;
            printf("Debug: '%s'\n", debug ? "ON" : "OFF");
        }
    }

    obj = my_json_get_key(json, "include");
    if(!obj) {
        fprintf(stderr, "ERROR: Include not found\n");
    } else {
        if(obj->type != json_array) {
            fprintf(stderr, "ERROR: Include is not an array\n");
        } else {
            len = obj->u.array.length;
            printf("Include:");
            for(i=0; i<len; i++) {
                include = obj->u.array.values[i];
                if(include->type != json_string) {
                    fprintf(stderr, "ERROR: Non-string value in include array\n");
                } else {
                    include_str = include->u.string.ptr;
                    printf("%s '%s'", i ? "," : "", include_str);
                }
            }
            printf("\n");
        }
    }

    ret = 0;
main_err:
    if(fd >= 0 && close(fd)) {
        /* If file has been opened, argv[1] must have been specified */
        fprintf(stderr, "ERROR: Cannot close '%s' file: %s\n", argv[1], strerror(errno));
    }
    if(json) {
        json_value_free(json);
    }
    return ret;
}

